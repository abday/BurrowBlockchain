//
//  CallRequest+BurrowBlockchain.swift
//  BurrowBlockchain
//
//  Created by Alex Tran-Qui on 20/04/2017.
//  Copyright © 2017 Katalysis / Alex Tran Qui (alex@katalysis.io). All rights reserved.
//
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.


// MARK: constant calls extension
extension BurrowBlockchain {
    private func asyncCall(to: Address, data: BCData, callback: @escaping (String) -> Void) -> RequestId {
        // TODO: this needs to be hooked
        let req = reqGen.next()
        self.send(msg: CallRequest(id: req, from: Address(key.account), to: to, data: data, callback: { response in
            let r = response as! CallResponse
            callback(r.result.returnV)
            return response
        }))
        return req
    }
    
    private func syncCall(to: Address, data: BCData) -> String {
        var holding = true
        var res = ""
        
        _ = self.asyncCall(to: to, data: data){ result in
            res = result
            holding = false
        }
        // wait until the value comes back
        socket.listenForResponse(isTrue: { return holding })
        return res
    }
    
    
    public func constCallContract(address: Address, functionCall: String) -> String {
        return syncCall(to: address, data: functionCall)
    }
}
