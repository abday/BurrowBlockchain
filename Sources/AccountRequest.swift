//
//  AccountRequest.swift
//  BurrowBlockchain
//
//  Created by Alex Tran-Qui on 30/09/2016.
//  Copyright © 2016-17 Katalysis / Alex Tran Qui (alex.tranqui@gmail.com). All rights reserved.
//
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//

import Foundation
import JSONCodable

// Accounts
public typealias GetAccountsParams = FiltersParams  // balance or code

public class GetAccountsRequest: BurrowMessage {
  public let method = "\(BURROWDB).getAccounts"
  public let params: GetAccountsParams

  public typealias Response = GetAccountsResponse
  public init(id: RequestId, params: GetAccountsParams, callback: @escaping (BurrowResponse) -> BurrowResponse = {r in return r}) {
    self.params = params
    super.init(id:id, callback: callback)
  }

  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    params = try decoder.decode("params")
    try super.init(object:object)
  }

  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(method, key: "method")
      try encoder.encode(params, key: "params")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }

  public override func parseResponse(JSONString: String) throws -> BurrowResponse {
    return try Response(JSONString: JSONString)
  }
}

public class GetAccountsResponse: BurrowResponse {
  public let result: Accounts

  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    result = try decoder.decode("result")
    try super.init(object: object)
  }

  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(result, key: "result")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }
}

public struct Accounts:JSONCodable {
  public let accounts: [Account]

  public init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    accounts = try decoder.decode("accounts")
  }

  public func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(accounts, key: "accounts")
    })
  }
}

public struct Permissions: JSONCodable {
  public let perms: PermFlag
  public let set: PermFlag
  public let roles: [String]

  public init(perms: PermFlag, set: PermFlag, roles: [String]) {
    self.perms = perms
    self.set = set
    self.roles = roles
  }

  public init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    perms = try decoder.decode("base.perms")
    set = try decoder.decode("base.set")
    roles = try decoder.decode("roles")
  }

  public func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(roles, key: "roles")
    })
    var base = JSONObject()
    base["perms"] = perms
    base["set"] = set
    res["base"] = base
    return res
  }
}



public struct Account: JSONCodable { // this represents a public account
  public let address: Address
  public let pub_key: PubKey?
  public let sequence: Int // also referred to as NONCE
  public let balance: UInt
  public let code: BCData
  public let storage_root: Address
  public let permissions: Permissions

    public init(_ address: Address, _ pub_key: PubKey?, _ sequence: Int, _ balance: UInt, _ code: BCData, _ storage_root: Address, _ permissions: Permissions) {
        self.address = address
        self.pub_key = pub_key
        self.sequence = sequence
        self.balance = balance
        self.code = code
        self.storage_root = storage_root
        self.permissions = permissions
    }
    
  public init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    address = try decoder.decode("address")
    do {
      let key: String = try decoder.decode("pub_key[1]")
      let type: KeyType = try decoder.decode("pub_key[0]")
      pub_key = PubKey(key: key,type: type)
    } catch {
      pub_key = nil
    }
    sequence = try decoder.decode("sequence")
    let b: Int = try decoder.decode("balance")
    balance = UInt(b)
    code = try decoder.decode("code")
    storage_root = try decoder.decode("storage_root")
    permissions = try decoder.decode("permissions")
  }

  public func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(address, key: "address")
      if (pub_key != nil) {
        try encoder.encode([pub_key!.type.rawValue, pub_key!.key] as [JSONEncodable], key: "pub_key")
      }
      try encoder.encode(sequence, key: "sequence")
      try encoder.encode(balance, key: "balance")
      try encoder.encode(code, key: "code")
      try encoder.encode(storage_root, key: "storage_root")
      try encoder.encode(permissions, key: "permissions")
    })
  }
}

//typealias GetAccountParams = AddressParams

public class GetAccountRequest: BurrowMessage { // TODO: check what returns when this is a private account (ie an account used to connect to the bc)
  public let method = "\(BURROWDB).getAccount"
  public let params: Address

  public typealias Response = GetAccountResponse
  public init(id: RequestId, address: Address, callback: @escaping (BurrowResponse) -> BurrowResponse = {r in return r}) {
    self.params = address
    super.init(id:id, callback:callback)
  }

  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    params = try decoder.decode("params.address")
    try super.init(object:object)
  }

  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(method, key: "method")
      try encoder.encode(["address": params] as [String: String], key: "params")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }

  public override func parseResponse(JSONString: String) throws -> BurrowResponse {
    return callback(try Response(JSONString: JSONString))
  }
}

public class GetAccountResponse: BurrowResponse {
  public let result: Account

  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    result = try decoder.decode("result")
    try super.init(object: object)
  }

  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(result, key: "result")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }
}
