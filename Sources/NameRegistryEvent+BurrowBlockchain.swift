//
//  NameRegistryEvent+BurrowBlockchain.swift
//  BurrowBlockchain
//
//  Created by Alex Tran-Qui on 20/04/2017.
//  Copyright © 2017 Katalysis / Alex Tran Qui (alex@katalysis.io). All rights reserved.
//
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

extension BurrowBlockchain {
    public func getNameRegistryValue(of: String) -> String {
        var holding = true
        var res = ""
        self.send(msg: GetNameRegEntryRequest(id: reqGen.next(), name: of,
                                              callback: { response in
                                                let r = response as! GetNameRegEntryResponse
                                                res = r.result.data
                                                holding = false
                                                return response
        }))
        socket.listenForResponse(isTrue: { return holding })
        return res
    }
    
    public func getNameRegistriesValue(of: [String]) -> [String:String] {
        var holding = true
        var res = [String:String]()
        var filters = [FilterData]()
        for s in of {
            filters.append(FilterData(field: "name", op: "==", value:s))
        }
        self.send(msg: GetNameRegEntriesRequest(id: reqGen.next(), params: FiltersParams(filters),
                                                callback: { response in
                                                    let r = response as! GetNameRegEntriesResponse
                                                    for n in r.result.names {
                                                        res[n.name] = n.data
                                                    }
                                                    holding = false
                                                    return response
        }))
        socket.listenForResponse(isTrue: { return holding })
        return res
    }
}
