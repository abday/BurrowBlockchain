//
//  BlockchainRequest+BurrowBlockchain.swift
//  BurrowBlockchain
//
//  Created by Alex Tran-Qui on 20/04/2017.
//  Copyright © 2017 Katalysis / Alex Tran Qui (alex@katalysis.io). All rights reserved.
//
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

// block request extension
extension BurrowBlockchain {
    /// MARK: Blockchain admin requests
    
    public func latestBlockHeight() -> Int {
        var holding  = true
        var h: Int = 0
        self.send(msg: GetLatestBlockHeightRequest(id: reqGen.next()) { response in
            let r = response as! GetLatestBlockHeightResponse
            h = r.height
            holding = false
            return response
        })
        socket.listenForResponse(isTrue: { return holding })
        return h
    }
    
    public func block(_ height: Int) -> Block? {
        var holding = true
        var b: Block?
        self.send(msg: GetBlockRequest(id: reqGen.next(), height: height) { response in
            let r = response as! GetBlockResponse
            b = r.result
            holding = false
            return response
        })
        socket.listenForResponse(isTrue: { return holding })
        return b
    }
    
    public func blocks(_ min: Int, _ max: Int) -> BlocksResult? {
        // This function does not work in 0.11.4 due to a bug (since fixed)
        let filtersParams = FiltersParams([FilterData(field:"height", op: ">=", value: "\(min)"), FilterData(field:"height", op: "<", value: "\(max)")])
        
        var holding = true
        var br: BlocksResult?
        self.send(msg: GetBlocksRequest(id: reqGen.next(), params: filtersParams) { response in
            let r = response as! GetBlocksResponse
            br = r.result
            holding = false
            return response
        })
        socket.listenForResponse(isTrue: { return holding })
        return br
    }
}
