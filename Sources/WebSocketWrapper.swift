//
//  WebSocketWrapper.swift
//  BurrowBlockchain
//
//  Created by Alex Tran-Qui on 06/10/2016.
//  Copyright © 2016-17 Katalysis / Alex Tran Qui (alex.tranqui@gmail.com). All rights reserved.
//
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//

import Foundation




#if os(iOS)
  import Starscream
  
  extension WebSocket: WebSocketWrapper {}
  
#endif

public protocol WebSocketWrapper {
  
  var onConnect: ((Void) -> Void)? {get set}
  var onDisconnect: ((NSError?) -> Void)? {get set}
  var onText: ((String) -> Void)? {get set}
  var onData: ((Data) -> Void)? {get set}
  var onPong: ((Data?) -> Void)? {get set}
  var isConnected: Bool {get}
  
  func write(string: String)
  func write(ping: Data)
  func write(string: String, completion: (() -> ())?)
  func write(ping: Data, completion: (() -> ())?)
  func connect()
  
  func listenForResponse(isTrue: () -> Bool, timeout: Double, process: (() -> Void)?)
  
}

extension WebSocketWrapper {
  public func write(string: String) {
    write(string: string, completion: nil)
  }
  
  public func write(ping: Data) {
    write(ping: ping, completion: nil)
  }
  
  func listenForResponse(isTrue: () -> Bool, timeout: Double) {
    listenForResponse(isTrue: isTrue, timeout: timeout, process: nil)
  }
  
  func listenForResponse(isTrue: () -> Bool) {
    listenForResponse(isTrue: isTrue, timeout: 30.0, process: nil)
  }
  
  
  #if os(iOS)
  
  public func listenForResponse(isTrue: () -> Bool, timeout: Double, process: (() -> Void)?) {
    var timesUp = false
    if #available(OSX 10.12, iOS 10, *) {
      let _ = Timer.scheduledTimer(withTimeInterval: timeout, repeats: false) { timer in
        timesUp = true
        timer.invalidate()
      }
    } else {
      print("No timeouts!")
    }
    
    while(isTrue() && !timesUp) {
      if (process != nil) {
        process!()
      }
      RunLoop.current.run(until: Date())
      usleep(10)
    }
  }
  
  #endif
}


