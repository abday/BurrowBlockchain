//
//  BlockchainRequest.swift
//  BurrowBlockchain
//
//  Created by Alex Tran-Qui on 30/09/2016.
//  Copyright © 2016-17 Katalysis / Alex Tran Qui (alex.tranqui@gmail.com). All rights reserved.
//
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//

import Foundation
import JSONCodable


public typealias BlockHeight = Int

// Blockchain
public class GetBlockchainInfoRequest: BurrowMessage {
  public let method = "\(BURROWDB).getBlockchainInfo"
  
  public typealias Response = GetBlockchainInfoResponse
  
  public override init(id: RequestId, callback: @escaping (BurrowResponse) -> BurrowResponse = {r in return r}) {
    super.init(id:id, callback: callback)
  }
  
  public required init(object: JSONObject) throws {
    try super.init(object:object)
  }
  
  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(method, key: "method")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }
  
  public override func parseResponse(JSONString: String) throws -> BurrowResponse {
    return callback(try Response(JSONString: JSONString))
  }
  
}


public class GetBlockchainInfoResponse: BurrowResponse {
  public let result: BlockchainInfo
  
  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    result = try decoder.decode("result")
    try super.init(object: object)
  }
  
  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(result, key: "result")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }
}

public struct BlockchainInfo: JSONCodable {
  public let chain_id: RequestId
  public let genesis_hash: BlockHash // first block on the chain. hash of the genesis block
  public let latest_block: BlockMeta
  public let latest_block_height: BlockHeight // this is also the height of the chain
  
  public init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    chain_id = try decoder.decode("chain_id")
    genesis_hash = try decoder.decode("genesis_hash")
    latest_block = try decoder.decode("latest_block")
    latest_block_height = try decoder.decode("latest_block_height")
  }
  
  public func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(chain_id, key: "chain_id")
      try encoder.encode(genesis_hash, key: "genesis_hash")
      try encoder.encode(latest_block, key: "latest_block")
      try encoder.encode(latest_block_height, key: "latest_block_height")
    })
  }
}

public struct BlockParts: JSONCodable {
  public let total: Int
  public let hash:  BlockHash
  
  public init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    total = try decoder.decode("total")
    hash = try decoder.decode("hash")
  }
  
  public func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(total, key: "total")
      try encoder.encode(hash, key: "hash")
    })
  }
}

public struct BlockHeader: JSONCodable {
  public let chain_id: RequestId
  public let height: BlockHeight
  public let time: Date
  public let fees: UInt
  public let num_txs: UInt
  public let last_block_hash: BlockHash
  public let last_block_parts: BlockParts
  public let state_hash: BlockHash
  
  public init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    chain_id = try decoder.decode("chain_id")
    height = try decoder.decode("height")
    time = try decoder.decode("time", transformer: JSONTransformers.StringToDate)
    fees = try decoder.decode("fees")
    num_txs = try decoder.decode("num_txs")
    last_block_hash = try decoder.decode("last_block_hash")
    last_block_parts = try decoder.decode("last_block_parts")
    state_hash = try decoder.decode("state_hash")
  }
  
  public func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(chain_id, key: "chain_id")
      try encoder.encode(height, key: "height")
      try encoder.encode(time, key: "time", transformer: JSONTransformers.StringToDate)
      
      try encoder.encode(fees, key: "fees")
      try encoder.encode(num_txs, key: "num_txs")
      try encoder.encode(last_block_hash, key: "last_block_hash")
      try encoder.encode(last_block_parts, key: "last_block_parts")
      try encoder.encode(state_hash, key: "state_hash")
    })
  }
}


public struct BlockMeta: JSONCodable {
  public let hash: BlockHash
  public let header: BlockHeader
  public let parts_header: BlockParts
  
  public init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    hash = try decoder.decode("hash")
    header = try decoder.decode("header")
    parts_header = try decoder.decode("parts_header")
  }
  
  public func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(hash, key: "hash")
      try encoder.encode(header, key: "header")
      try encoder.encode(parts_header, key: "parts_header")
    })
  }
}


public class GetGenesisHashRequest: BurrowMessage {
  public let method = "\(BURROWDB).getGenesisHash"
  
  public typealias Response = GetGenesisHashResponse
  
  public override init(id: RequestId, callback: @escaping (BurrowResponse) -> BurrowResponse = {r in return r}) {
    super.init(id:id, callback: callback)
  }
  
  public required init(object: JSONObject) throws {
    try super.init(object:object)
  }
  
  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(method, key: "method")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }
  
  public override func parseResponse(JSONString: String) throws -> BurrowResponse {
    return callback(try Response(JSONString: JSONString))
  }
  
}

public class GetGenesisHashResponse: BurrowResponse {
  public let hash: BlockHash
  
  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    hash = try decoder.decode("result.hash")
    try super.init(object: object)
  }
  
  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(["hash": hash] as [String: BlockHash], key: "result")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }
}


public class GetLatestBlockHeightRequest: BurrowMessage {
  public let method = "\(BURROWDB).getLatestBlockHeight"
  
  public typealias Response = GetLatestBlockHeightResponse
  
  public override init(id: RequestId, callback: @escaping (BurrowResponse) -> BurrowResponse = {r in return r}) {
    super.init(id:id, callback: callback)
  }
  
  public required init(object: JSONObject) throws {
    try super.init(object:object)
  }
  
  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(method, key: "method")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }
  
  public override func parseResponse(JSONString: String) throws -> BurrowResponse {
    return callback(try Response(JSONString: JSONString))
  }
}

public class GetLatestBlockHeightResponse: BurrowResponse {
  public let height: BlockHeight
  
  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    height = try decoder.decode("result.height")
    try super.init(object: object)
  }
  
  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(["height": height] as [String: BlockHeight], key: "result")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }
}

public class GetLatestBlockRequest: BurrowMessage {
  public let method = "\(BURROWDB).getLatestBlock"
  
  public typealias Response = GetBlockResponse
  public override init(id: RequestId, callback: @escaping (BurrowResponse) -> BurrowResponse = {r in return r}) {
    super.init(id:id, callback: callback)
  }
  
  public required init(object: JSONObject) throws {
    try super.init(object:object)
  }
  
  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(method, key: "method")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }
  
  public override func parseResponse(JSONString: String) throws -> BurrowResponse {
    return callback(try Response(JSONString: JSONString))
  }
}

public class GetBlockResponse: BurrowResponse {
  public let result: Block
  
  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    result = try decoder.decode("result")
    try super.init(object: object)
  }
  
  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(result, key: "result")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }
}

public typealias GetBlocksParams = FiltersParams // TODO: list all possible filters

public class GetBlocksRequest: BurrowMessage {
  
  public let method = "\(BURROWDB).getBlocks"
  public let params: GetBlocksParams
  
  public typealias Response = GetBlocksResponse
  public init(id: RequestId, params: GetBlocksParams, callback: @escaping (BurrowResponse) -> BurrowResponse = {r in return r}) {
    self.params = params
    super.init(id:id, callback: callback)
  }
  
  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    params = try decoder.decode("params")
    try super.init(object:object)
  }
  
  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(method, key: "method")
      try encoder.encode(params, key: "params")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }
  
  public override func parseResponse(JSONString: String) throws -> BurrowResponse {
    return callback(try Response(JSONString: JSONString))
  }
}

public class GetBlocksResponse: BurrowResponse {
  public let result: BlocksResult
  
  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    result = try decoder.decode("result")
    try super.init(object: object)
  }
  
  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(result, key: "result")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }
}

public struct BlocksResult: JSONCodable {
  public let min_height: BlockHeight
  public let max_height: BlockHeight
  public let latest_block: [BlockMeta]
  
  public init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    min_height = try decoder.decode("min_height")
    max_height = try decoder.decode("max_height")
    latest_block = try decoder.decode("latest_block")
  }
  
  public func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(min_height, key: "min_height")
      try encoder.encode(max_height, key: "max_height")
      try encoder.encode(latest_block, key: "latest_block")
    })
  }
}

public class GetBlockRequest: BurrowMessage {
  public let method = "\(BURROWDB).getBlock"
  public let height: BlockHeight
  
  public typealias Response = GetBlockResponse
  public init(id: RequestId, height: BlockHeight, callback: @escaping (BurrowResponse) -> BurrowResponse = {r in return r}) {
    self.height = height
    super.init(id:id, callback: callback)
  }
  
  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    height = try decoder.decode("params/height")
    try super.init(object:object)
  }
  
  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(method, key: "method")
      try encoder.encode(["height": height] as [String: BlockHeight], key: "params")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }
  
  public override func parseResponse(JSONString: String) throws -> BurrowResponse {
    return callback(try Response(JSONString: JSONString))
  }
}

public struct Block: JSONCodable {
  public let header: BlockHeader
  public let last_validation: BlockValidation
  private var _txs: [Tx]
  
  public var txs: [Tx] {
    get {
      return _txs
    }
  }
  
  public init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    header = try decoder.decode("header")
    last_validation = try decoder.decode("last_validation")
    _txs = [Tx]()
    for i in 0..<header.num_txs {
      let type: Int = try decoder.decode("data.txs[\(i)][0]")
      switch(TransactionType(rawValue:type)!) {
      case .TxTypeCall:
        let t : CallTx = try decoder.decode("data.txs[\(i)][1]")
        _txs.append(t)
      case .TxTypeName:
        let t : NameTx = try decoder.decode("data.txs[\(i)][1]")
        _txs.append(t)
      case .TxTypePermissions:
        let t : PermissionTx = try decoder.decode("data.txs[\(i)][1]")
        _txs.append(t)
      case .TxTypeBond:
        let t : BondTx = try decoder.decode("data.txs[\(i)][1]")
        _txs.append(t)
      case .TxTypeSend:
        let t : SendTx = try decoder.decode("data.txs[\(i)][1]")
        _txs.append(t)
      case .TxTypeUnbond:
        let t : UnbondTx = try decoder.decode("data.txs[\(i)][1]")
        _txs.append(t)
      case .TxTypeRebond:
        let t : RebondTx = try decoder.decode("data.txs[\(i)][1]")
        _txs.append(t)
      case .TxTypeDupeout:
        let t : DupeoutTx = try decoder.decode("data.txs[\(i)][1]")
        _txs.append(t)
      }
    }
  }
  
  public func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(header, key: "header")
      try encoder.encode(last_validation, key: "last_validation")
    })
    //res["data"] = JSONObject()
    var t = [Any]()
    for e in _txs {
      t.append(try e.toJSON())
    }
    res["data"] = ["txs": t]
    
    return res
  }
  
}

// TODO: can be absorbed
public struct BlockValidation: JSONCodable {
  public let precommits: [BlockCommit]
  
  
  public init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    precommits = try decoder.decode("precommits")
  }
  
  public func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(precommits, key: "precommits")
    })
  }
}


public struct BlockCommit: JSONCodable {
  public let height: BlockHeight
  public let round: Int
  public let type: Int  // TODO: what is this type?
  public let block_hash: BlockHash
  public let block_parts_header: BlockParts
  public let signature: Signature
  
  public init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    height = try decoder.decode("height")
    round = try decoder.decode("round")
    type = try decoder.decode("type")
    block_hash = try decoder.decode("block_hash")
    block_parts_header = try decoder.decode("block_parts_header")
    let sig: String = try decoder.decode("signature[1]")
    let sigtype: KeyType = try decoder.decode("signature[0]")
    signature = Signature(sig, type: sigtype)
  }
  
  public func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(height, key: "height")
      try encoder.encode(round, key: "round")
      try encoder.encode(type, key: "type")
      try encoder.encode(block_hash, key: "block_hash")
      try encoder.encode(block_parts_header, key: "block_parts_header")
      try encoder.encode([signature.type.rawValue, signature.sig] as [JSONEncodable], key: "signature")
    })
  }
}
