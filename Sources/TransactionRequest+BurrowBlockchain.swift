//
//  TransactionRequest+BurrowBlockchain.swift
//  BurrowBlockchain
//
//  Created by Alex Tran-Qui on 20/04/2017.
//  Copyright © 2017 Katalysis / Alex Tran Qui (alex@katalysis.io). All rights reserved.
//
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.


// MARK: transaction calls extension
extension BurrowBlockchain {
    
    private func asyncTransact(tx: CallTx, callback: @escaping (String) -> Void) -> RequestId {
        var holding = true
        var tx_hash = ""
        self.send(msg: BroadcastTxRequest(id: reqGen.next(), tx: tx, callback: { response in
            let r = response as! BroadcastTxResponse
            tx_hash = r.result.tx_hash
            holding = false
            return response
        }))
        
        // wait for the transaction hash to come back
        socket.listenForResponse(isTrue: { return holding })
        
        var sub_id = ""
        self.send(msg: EventSubscribeRequest(id: self.reqGen.next(), event_id: EventCall.request(address: tx.to), callback: { response in
            
            let r = response as! EventSubscribeResponse
            sub_id = r.result // sub_id
            
            // add an entry to the openRequests dic
            // TODO: add an entry in the event entry?
            self.openRequests[sub_id] = EventCall(id: sub_id,callback: { response in
                let r = response as! EventCallResponse
                // TODO: is this sufficient to identify the correct transaction?
                if (r.tx_id == tx_hash /* && r.type == EventDataType.call.rawValue */ && r.call_data.data == tx.data) { // check that this event is related to the event we are processing. is this necessary?
                    // Unsubscribe
                    self.send(msg: EventUnsubscribeRequest(id: self.reqGen.next(), event_id: sub_id))
                    callback(r.returnV)
                }
                return response
            })
            return response
        }))
        return sub_id
    }
    
    
    private func syncTransact(tx: CallTx) -> String {
        // https://github.com/eris-ltd/eris-db/blob/develop/manager/eris-mint/transactor.go
        var res = ""
        var holding = true
        
        _ = asyncTransact(tx: tx) { result in
            res = result
            holding = false
        }
        
        // wait for the response to come back
        socket.listenForResponse(isTrue: { return holding })
        
        return res // should return whatever comes back from the call
    }
    
    
    public func callContract(address: Address, functionCall: String, amount: Int = 1, gas_limit: Int = 1000000000, fee: Int = 1) -> String {
        let i = TxInput(from: key.account, amount: amount, pubKey: key.pubKeyStr)
        let ctx = CallTx(input: i, to: address, gas_limit: gas_limit, fee: fee, data: functionCall)
        
        return syncTransact(tx: ctx)
    }
    
    
}
