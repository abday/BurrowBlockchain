//
//  EventRequest.swift
//  BurrowBlockchain
//
//  Created by Alex Tran-Qui on 30/09/2016.
//  Copyright © 2016-17 Katalysis / Alex Tran Qui (alex.tranqui@gmail.com). All rights reserved.
//
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//

import Foundation
import JSONCodable

// Events
public class EventSubscribeRequest: BurrowMessage {
    public let method = "\(BURROWDB).eventSubscribe"
    public let event_id: String
    
    public typealias Response = EventSubscribeResponse
    public init(id: RequestId, event_id: String, callback: @escaping (BurrowResponse) -> BurrowResponse = {r in return r}) {
        self.event_id = event_id
        super.init(id:id, callback:callback)
    }
    
    public required init(object: JSONObject) throws {
        let decoder = JSONDecoder(object: object)
        event_id = try decoder.decode("params.event_id")
        try super.init(object:object)
    }
    
    public override func toJSON() throws -> Any {
        var res = try JSONEncoder.create({ (encoder) in
            try encoder.encode(method, key: "method")
            try encoder.encode(["event_id": event_id] as [String: String], key: "params")
        })
        for e in (try super.toJSON()) as! JSONObject {
            res[e.key] = e.value
        }
        return res
    }
    
    public override func parseResponse(JSONString: String) throws -> BurrowResponse {
        return callback(try Response(JSONString: JSONString))
    }
}

public class EventSubscribeResponse: BurrowResponse {
    public let result: RequestId
    
    
    public required init(object: JSONObject) throws {
        let decoder = JSONDecoder(object: object)
        result = try decoder.decode("result.sub_id")
        try super.init(object: object)
    }
    
    public override func toJSON() throws -> Any {
        var res = try JSONEncoder.create({ (encoder) in
            try encoder.encode(["sub_id": result] as [String: RequestId], key: "result")
        })
        for e in (try super.toJSON()) as! JSONObject {
            res[e.key] = e.value
        }
        return res
    }
}

public class EventUnsubscribeRequest: BurrowMessage {
    public let method = "\(BURROWDB).eventUnsubscribe"
    public let event_id: String
    
    public typealias Response = EventUnsubscribeResponse
    public init(id: RequestId, event_id: String, callback: @escaping (BurrowResponse) -> BurrowResponse = {r in return r}) {
        self.event_id = event_id
        super.init(id:id, callback:callback)
    }
    
    public required init(object: JSONObject) throws {
        let decoder = JSONDecoder(object: object)
        event_id = try decoder.decode("params.event_id")
        try super.init(object:object)
    }
    
    public override func toJSON() throws -> Any {
        var res = try JSONEncoder.create({ (encoder) in
            try encoder.encode(method, key: "method")
            try encoder.encode(["event_id": event_id] as [String: String], key: "params")
        })
        for e in (try super.toJSON()) as! JSONObject {
            res[e.key] = e.value
        }
        return res
    }
    
    public override func parseResponse(JSONString: String) throws -> BurrowResponse {
        return callback(try Response(JSONString: JSONString))
    }
}


public class EventUnsubscribeResponse: BurrowResponse {
    public let result: Bool
    
    public required init(object: JSONObject) throws {
        let decoder = JSONDecoder(object: object)
        result = try decoder.decode("result.result")
        try super.init(object: object)
    }
    
    public override func toJSON() throws -> Any {
        var res = try JSONEncoder.create({ (encoder) in
            try encoder.encode(["result": result] as [String: Bool], key: "result")
        })
        for e in (try super.toJSON()) as! JSONObject {
            res[e.key] = e.value
        }
        return res
    }
}

// TODO: define all the event types and structures as enums?
// https://github.com/eris-ltd/eris-db/blob/develop/txs/events.go


public enum EventDataType: Int {
    case newBlock = 0x01
    case fork     = 0x02
    case tx       = 0x03
    case call     = 0x04
    case log      = 0x05
    case newBlockHeader = 0x06
    
    case roundState = 0x11
    case vote       = 0x12
}

public class EventResponse: JSONCodable {
    public let type: EventDataType
    
    public init(type: EventDataType) {
        self.type = type
    }
    
    public required init(object: JSONObject) throws {
        type = .newBlock // TODO: hacked default, to remove
    }
    
    public func toJSON() throws -> Any {
        return try JSONEncoder.create({ (encoder) in
            try encoder.encode(type, key: "type") // TODO: hacked default, to remove
        })
    }
    
}
public class EventInput: BurrowMessage {
    // TODO: look at having BurrowMessage being a protocol?
    public static func request(address: Address) -> String {
        return "Acc/\(address)/Input"
    }
    
    public override init (id: RequestId, callback: @escaping (BurrowResponse) -> BurrowResponse) {
        super.init(id:id, callback:callback)
    }
    
    public required init(object: JSONObject) throws {
        fatalError("init(object:) has not been implemented")
    }
    
    public typealias Response = EventInputResponse
    
    public override func parseResponse(JSONString: String) throws -> BurrowResponse {
        return callback(try Response(JSONString: JSONString))
    }
}


public class EventInputResponse: BurrowResponse {
    public let type: Int
    public let tx: Tx
    public let returnV: String
    public let exception: String
    
    public required init(object: JSONObject) throws {
        let decoder = JSONDecoder(object: object)
        type = try decoder.decode("result[0]")
        tx = try decoder.decode("result[1].tx")
        returnV = try decoder.decode("result[1].return")
        exception = try decoder.decode("result[1].exception")
        try super.init(object: object)
    }
    
    public override func toJSON() throws -> Any {
        return try JSONEncoder.create({ (encoder) in
            // TODO: fix: add type in array
            try encoder.encode(tx, key: "tx")
            try encoder.encode(returnV, key: "return")
            try encoder.encode(exception, key: "exception")
        })
    }
}

public class EventOutput: BurrowResponse {
    
}

public struct CallData: JSONCodable {
    public let caller: String
    public let callee: String
    public let data: String
    public let value: Int
    public let gas: Int
    
    public init(object: JSONObject) throws {
        let decoder = JSONDecoder(object: object)
        caller = try decoder.decode("caller")
        callee = try decoder.decode("callee")
        data = try decoder.decode("data")
        value = try decoder.decode("value")
        gas = try decoder.decode("gas")
    }
    
    public func toJSON() throws -> Any {
        return try JSONEncoder.create({ (encoder) in
            try encoder.encode(caller, key: "caller")
            try encoder.encode(callee, key: "callee")
            try encoder.encode(data, key: "data")
            try encoder.encode(value, key: "value")
            try encoder.encode(gas, key: "gas")
        })
    }
}




public class EventCall: BurrowMessage {
    // TODO: look at having BurrowMessage being a protocol?
    public static func request(address: Address) -> String {
        return "Acc/\(address)/Call"
    }
    
    public override init (id: RequestId, callback: @escaping (BurrowResponse) -> BurrowResponse) {
        super.init(id:id, callback:callback)
    }
    
    public required init(object: JSONObject) throws {
        fatalError("init(object:) has not been implemented")
    }
    
    public typealias Response = EventCallResponse
    
    public override func parseResponse(JSONString: String) throws -> BurrowResponse {
        return callback(try Response(JSONString: JSONString))
    }
}

public class EventCallResponse: BurrowResponse {
    public let call_data: CallData
    public let origin: String
    public let tx_id: String
    public let returnV: String
    public let exception: String
    
    public required init(object: JSONObject) throws {
        let decoder = JSONDecoder(object: object)
        call_data = try decoder.decode("result.call_data")
        origin = try decoder.decode("result.origin")
        tx_id = try decoder.decode("result.tx_id")
        returnV = try decoder.decode("result.return")
        exception = try decoder.decode("result.exception")
        try super.init(object: object)
    }
    
    public override func toJSON() throws -> Any {
        return try JSONEncoder.create({ (encoder) in
            // TODO: fix: add type in array
            try encoder.encode(call_data, key: "call_data")
            try encoder.encode(origin, key: "origin")
            try encoder.encode(tx_id, key: "tx_id")
            try encoder.encode(returnV, key: "return")
            try encoder.encode(exception, key: "exception")
        })
    }
}

public class EventLog: BurrowMessage {
    // TODO: look at having BurrowMessage being a protocol?
    public static func request(address: Address) -> String {
        return "Log/\(address)"
    }
    
    public override init (id: RequestId, callback: @escaping (BurrowResponse) -> BurrowResponse) {
        super.init(id:id, callback:callback)
    }
    
    public required init(object: JSONObject) throws {
        fatalError("init(object:) has not been implemented")
    }
    
    public typealias Response = EventLogResponse
    
    public override func parseResponse(JSONString: String) throws -> BurrowResponse {
        return callback(try Response(JSONString: JSONString))
    }
}

public class EventLogResponse: BurrowResponse {
    public let type: Int  = 0x05 // TODO: look into using EventResponse to manage type
    public let address: String
    public let topics: [String]
    public let data: String
    public let height: UInt
    
    public required init(object: JSONObject) throws {
        let decoder = JSONDecoder(object: object)
        //type = try decoder.decode("result[0]")
        address = try decoder.decode("result.address")
        topics = try decoder.decode("result.topics") // array of up to 4 items
        data = try decoder.decode("result.data") //
        let h: Int = try decoder.decode("result.height")
        height = UInt(h)
        try super.init(object: object)
    }
    
    public override func toJSON() throws -> Any {
        return try JSONEncoder.create({ (encoder) in
            // TODO: fix: add type in array
            try encoder.encode(address, key: "address")
            try encoder.encode(topics, key: "topics")
            try encoder.encode(data, key: "data")
            try encoder.encode(height, key: "height")
        })
    }
}



public class EventNewBlock: BurrowMessage {
    public static func request() -> String {
        return "NewBlock"
    }
    
    public override init (id: RequestId, callback: @escaping (BurrowResponse) -> BurrowResponse) {
        super.init(id:id, callback:callback)
    }
    
    public required init(object: JSONObject) throws {
        fatalError("init(object:) has not been implemented")
    }
    
    public typealias Response = EventNewBlockResponse
    
    public override func parseResponse(JSONString: String) throws -> BurrowResponse {
        return callback(try Response(JSONString: JSONString))
    }
}


public class EventNewBlockResponse: BurrowResponse {
    public let type: Int = 0x01// TODO: look into using EventResponse to manage type
    public let block: Block
    
    public required init(object: JSONObject) throws {
        let decoder = JSONDecoder(object: object)
        //type = try decoder.decode("result[0]")
        block = try decoder.decode("result.block")
        try super.init(object: object)
    }
    
    public override func toJSON() throws -> Any {
        return try JSONEncoder.create({ (encoder) in
            // TODO: fix: add type in array
            try encoder.encode(block, key: "block")
        })
    }
}

/*
 
 
 public class EventPermissions: EventResponse {
 static func request(name: String) -> String {
 return "Permissions/\(name)"
 }
 }
 
 public class EventNameReg: EventResponse {
 static func request(name: String) -> String {
 return "NameReg/\(name)"
 }
 }
 
 public class EventNewBlock: EventResponse {
 static func request() -> String {
 return "NewBlock"
 }
 }
 
 public class EventFork: EventResponse {
 static func request() -> String {
 return "Fork"
 }
 }
 
 public class EventBond: EventResponse {
 static func request() -> String {
 return "Bond"
 }
 }
 
 public class EventUnbond: EventResponse {
 static func request() -> String {
 return "Unbond"
 }
 }
 
 public class EventRebond: EventResponse {
 static func request() -> String {
 return "Rebond"
 }
 }
 
 public class EventDupeOut: EventResponse {
 static func request() -> String {
 return "Dupeout"
 }
 }
 
 
 
 */
