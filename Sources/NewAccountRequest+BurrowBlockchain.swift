//
//  NewAccountRequest+BurrowBlockchain.swift
//  BurrowBlockchain
//
//  Created by Alex Tran-Qui on 20/04/2017.
//  Copyright © 2017 Katalysis / Alex Tran Qui (alex@katalysis.io). All rights reserved.
//
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.


// MARK: Account creation extention
extension BurrowBlockchain {
    
    // this call is only relevant when you go through the Eris Proxy
    public func requestAccount(account: String) {
        let req = NewAccountRequest(id: reqGen.next(), account: account)
        self.send(msg: req)
    }
    
    public func createAccount(account: String, available: Int = 1000000, amount: Int = 1000000, _ permFlag: PermFlag = [.call, .send]) {
        // this assumes that self.key has the relevant permissions
        // it also assumes that the account doesn't exist
        let input1 = TxInput(from: self.key.account, amount: available, pubKey: self.key.pubKeyStr)
        let out = TxOutput(address: account, amount: amount)
        let sendTx = SendTx(inputs: [input1], outputs: [out])
        self.send(msg: BroadcastTxRequest(id: reqGen.next(), tx: sendTx))
        let input2 = TxInput(from: self.key.account, amount: available, pubKey: key.pubKeyStr)
        let perm = SetBaseArgs(address: account, perm: permFlag.rawValue, value: true)
        let permissionTx = PermissionTx(input: input2, args: perm)
        self.send(msg: BroadcastTxRequest(id: reqGen.next(), tx: permissionTx))
    }
}

