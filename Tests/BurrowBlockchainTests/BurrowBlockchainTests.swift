import XCTest
import ErisKeys
@testable import BurrowBlockchain

// this is the URL of the blockchain
var url = URL(string: "ws://ebook1.katalysis.nl:8081")!
// this is the private key seed for an account on that blockchain
let seed = [UInt8]("5AA78B7821858CEFFB72E1D13C571F1BE74839F23F0C602B7C432F040076B66A".toByteArray()!)
let bc = BurrowBlockchain(url: url, key: ErisKey(seed), "Test", true, true)

// this is the name of the chain on which you are testing
let chainId = ""
//let account = Account()



class BurrowBlockchainTests: XCTestCase {
    
    override init() {
        bc.initialize()
        super.init()
    }
    
    func testChainId() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        XCTAssertEqual("", "Hello, World!")
    }
    
    func testGetAccounts() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        XCTAssertEqual("", "Hello, World!")
        
    }
    
    func testGetAccountExternal() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        XCTAssertEqual("", "Hello, World!")
    }
    
    func testGetAccountContract() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        XCTAssertEqual("", "Hello, World!")
    }
    
    func testError() {
        
    }

    func testWrongSequenceRetry() {
        
    }
    

    static var allTests = [
        ("testChainId", testChainId),
        ("testGetAccounts", testGetAccounts),
        ("testGetAccountExternal", testGetAccountExternal),
        ("testGetAccountContract", testGetAccountContract),
        ("testError", testError),
        ("testWrongSequenceRetry", testWrongSequenceRetry),

    ]
}
