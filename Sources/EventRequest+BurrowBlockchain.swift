//
//  EventRequest+BurrowBlockchain.swift
//  BurrowBlockchain
//
//  Created by Alex Tran-Qui on 20/04/2017.
//  Copyright © 2017 Katalysis / Alex Tran Qui (alex@katalysis.io). All rights reserved.
//
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.


// MARK: Event management extension
extension BurrowBlockchain {
    
    public func subscribeTo(event e: String, at address: Address , callback: @escaping (BurrowResponse) -> Void) {
        // TODO: check if event already subscribed
        // if so, then just add new entry in the filter list
        // if not, subcribe to the event as well as adding a new entry
        if (openEvents[address] == nil) {
            let events: [String: (BurrowResponse) -> Void] = [e: callback]
            openEvents[address] = events
        } else {
            openEvents[address]![e] = callback
        }
        self.send(msg: EventSubscribeRequest(id: self.reqGen.next(), event_id: EventLog.request(address: address), callback: { response in
            
            let r = response as! EventSubscribeResponse
            let sub_id = r.result // sub_id
            self.subscriptions[address] = sub_id
            // add an entry to the openRequests dic
            self.openRequests[sub_id] = EventLog(id: sub_id,callback: { response in
                let r = response as! EventLogResponse
                self.openEvents[address]![e]!(r)
                return response
            })
            return response
        }))
    }
    
    public func unsubscribeTo(event e: String, at address: Address) {
        if (openEvents[address] != nil) {
            openEvents[address]![e] = nil
            if (openEvents[address]!.isEmpty) {
                self.send(msg: EventUnsubscribeRequest(id: self.reqGen.next(), event_id: subscriptions[address]!))
            }
        }
    }
    
    public func subscribeToNewBlock(callback: @escaping (EventNewBlockResponse) -> Void) {
        // TODO: implement
        self.send(msg: EventSubscribeRequest(id: self.reqGen.next(), event_id: EventNewBlock.request(), callback: { response in
            
            let r = response as! EventSubscribeResponse
            let sub_id = r.result // sub_id
            // TODO: store sub_id for future unsubscription
            self.subscriptions["Block"] = sub_id
            // add an entry to the openRequests dic
            self.openRequests[sub_id] = EventNewBlock(id: sub_id,callback: { response in
                let r = response as! EventNewBlockResponse
                callback(r)
                return response
            })
            return response
        }))
        
    }
    
    public func unsubscribeToNewBlock() {
        if (subscriptions["Block"] != nil) {
            self.send(msg: EventUnsubscribeRequest(id: self.reqGen.next(), event_id: subscriptions["Block"]!))
        }
    }
}
