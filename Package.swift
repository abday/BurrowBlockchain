import PackageDescription
#if os(Linux) || os(OSX)
let package = Package(
    name: "BurrowBlockchain",
    dependencies: [
      .Package(url: "https://gitlab.com:katalysis/ErisKeys.git", majorVersion: 0, minor: 3),
      .Package(url: "https://gitlab.com:katalysis/JSONCodable.git", majorVersion: 3, minor: 2),
      .Package(url: "https://github.com/Zewo/WebSocketClient.git", majorVersion: 0, minor: 14),
    ]
)
#else
let package = Package(
  name: "BurrowBlockchain",
  dependencies: [
    .Package(url: "https://gitlab.com:katalysis/ErisKeys.git", majorVersion: 0, minor: 3),
    .Package(url: "https://gitlab.com:katalysis/JSONCodable.git", majorVersion: 3, minor: 2),
    .Package(url: "https://gitlab.com:katalysis/Starscream.git", majorVersion: 2, minor: 1),
  ]
)
#endif
